#include <iostream>
#include "Pila.h"

using namespace std;

Pila::Pila(int max){
    this->max = max;
    this->arreglo_pila = new int [this->max];
}


void Pila::Pila_vacia(){
    if(this->tope == 0){
        // La pila esta vacia
        this->band = true; 
    }
    
    else{
        // La pila esta llena
        this->band = false;
    }

}


void Pila::Pila_llena(){
    if(this->tope == this->max){
        // La pila esta vacia
        this->band = true;
    }
    
    else{
        // La pila no esta llena
        this->band = false;
    }
    
}


void Pila::Push(int dato){
    Pila_llena();

    if (this->band == true){
        cout << "Desbordamiento, Pila llena" << endl;
    }
    
    else{
        this->tope = this->tope + 1;
        // Se añade el elemento en el nuevo tope
        this->arreglo_pila[this->tope] = dato;
    }
}


void Pila::Pop(int dato){
    Pila_vacia();

    if (this->band == true){
        cout << "Subdesbordamiento, Pila vacı́a";
    }
    
    else{
        dato = this->arreglo_pila[this->tope];
        this->tope = this->tope - 1;
    }
}


void Pila::Imprimir_arreglo_pila(){
    cout << "\tPILA\n" << endl;
    
    // El último elemento agregado es el primero que se imprime
    for(int i = this->tope; i >= 1; i--){
        cout << "\t|" << arreglo_pila[i] << "|" << endl;
    }
}


