#include <iostream>

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
    private:
        int tope = 0;
        int max;
        int *arreglo_pila = NULL;
        int dato;
        bool band;

    public:
        Pila(int max);
        void Pila_vacia();
        void Pila_llena();
        void Push(int dato);
        void Pop(int dato);
        void Imprimir_arreglo_pila();

};
#endif
