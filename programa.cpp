#include <iostream>
#include "Pila.h"

using namespace std;


void menu(Pila pila){
    int opcion;
    int dato;
    
    cout << "\nMENU\n" << endl;
    cout << " [1] Agregar elementos\n [2] Eliminar elementos\n [3] Ver pila" << endl;
    cout << " [4] Salir" << endl;
     
    cout << "\n Ingrese su opción: ";
    cin >> opcion;
    
    system("clear");

    while(getchar() != '\n');

    switch(opcion){
        case 1:
            cout << "Ingrese un número: " << endl;
            cin >> dato;

            pila.Push(dato);
            menu(pila);
            break;

        case 2:
            cout << "Ingrese un número: " << endl;
            cin >> dato;

            pila.Pop(dato);
            menu(pila);
            break;

        case 3:
            pila.Imprimir_arreglo_pila();
            menu(pila);
            break;

        case 4:
            break;

    }
}

    
int main(){
    int max = 0;

    cout << "Tamaño máximo de la pila:" << endl;
    cin >> max;

    Pila pila = Pila(max);

    menu(pila);
   
    return 0;
} 
